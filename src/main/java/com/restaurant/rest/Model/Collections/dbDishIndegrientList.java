package com.restaurant.rest.Model.Collections;

import com.restaurant.rest.Model.dbDishIndegrient;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Dominik on 19.09.2017.
 */
@XmlRootElement
public class dbDishIndegrientList {

    public dbDishIndegrientList(List<dbDishIndegrient> dbDishIndegrients) {
        this.dbDishIndegrients = dbDishIndegrients;
    }

    public dbDishIndegrientList() {
    }

    @XmlElementWrapper
    @XmlElement(name="dbDishIndegrient")
    public List<dbDishIndegrient> getDbDishIndegrients() {
        return dbDishIndegrients;
    }

    public void setDbDishIndegrients(List<dbDishIndegrient> dbDishIndegrients) {
        this.dbDishIndegrients = dbDishIndegrients;
    }

    private List<dbDishIndegrient> dbDishIndegrients;

}
