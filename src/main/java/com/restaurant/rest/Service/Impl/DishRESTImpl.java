package com.restaurant.rest.Service.Impl;

import com.restaurant.rest.EJB.DishEJB;
import com.restaurant.rest.Model.Collections.dbDishList;
import com.restaurant.rest.Model.dbDish;
import com.restaurant.rest.Service.DishREST;

import javax.ejb.EJB;
import javax.ws.rs.*;

/**
 * Created by Dominik on 06.09.2017.
 */
@Path("/dish")
@Produces({"application/xml"})
@Consumes({"application/xml"})
public class DishRESTImpl implements DishREST {

    @EJB
    DishEJB dishEJB;

    @Override
    @GET
    @Path("/get")
    public dbDishList get() {
        return dishEJB.findAll();

    }


    @Override
    @POST
    @Path("/create")
    public dbDish postDish(dbDish dish) {
        try {
            dbDish dish1 = dishEJB.create(dish);
            return dish1;
        }
        catch (Exception ex){
            System.out.print(ex.getMessage());
        }
        return null;

    }

    @Override
    @POST
    @Path("/update")
    public dbDish updateDish(dbDish dish) {
        try {
           return dishEJB.update(dish);
        }
        catch (Exception ex){
            System.out.print(ex.getMessage());
            return null;
        }
    }
}
