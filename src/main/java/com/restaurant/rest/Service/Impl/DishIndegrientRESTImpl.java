package com.restaurant.rest.Service.Impl;

import com.restaurant.rest.EJB.DishEJB;
import com.restaurant.rest.EJB.DishIndegrientEJB;
import com.restaurant.rest.Model.Collections.dbDishIndegrientList;
import com.restaurant.rest.Model.dbDish;
import com.restaurant.rest.Model.dbDishIndegrient;
import com.restaurant.rest.Service.DishIndegrientREST;

import javax.ejb.EJB;
import javax.ws.rs.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Dominik on 16.09.2017.
 */
@Path("/dishindegrient")
@Produces({"application/xml"})
@Consumes({"application/xml"})
public class DishIndegrientRESTImpl implements DishIndegrientREST {

    @EJB
    private DishIndegrientEJB dishIndegrientEJB;
    @EJB
    private DishEJB dishEJB;

    @Override
    public List<dbDishIndegrient> get() {
        return null;
    }

    @Override
    public dbDish postDish(dbDishIndegrient dish) {
        return null;
    }

    @Override
    @POST
    @Path("/bulkupdate")
    public void updateDishIndegrient(dbDishIndegrientList dbDishIndegrientList) {

        for (dbDishIndegrient dbDishIndegrient : dbDishIndegrientList.getDbDishIndegrients()){
            dishIndegrientEJB.update(dbDishIndegrient);
        }

    }
    @Override
    @POST
    @Path("/getindegrientsfordish")
    public dbDishIndegrientList getIndegrientForDish(dbDish dish) {
        dbDish newDish =  dishEJB.findDishById(dish.getId());
        dbDishIndegrientList indegrientList = new dbDishIndegrientList(
               new LinkedList<>(newDish.getDbDishIndegrientCollection()));
        return indegrientList;
    }


}
