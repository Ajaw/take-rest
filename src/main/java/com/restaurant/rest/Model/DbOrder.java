/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.restaurant.rest.Model;


import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Dominik
* @version 1.0
 */
@Entity
@Table(name = "dbOrder")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DbOrder.findAll", query = "SELECT d FROM DbOrder d")
    , @NamedQuery(name = "DbOrder.findById", query = "SELECT d FROM DbOrder d WHERE d.id = :id")
    , @NamedQuery(name = "DbOrder.findByOrderDate", query = "SELECT d FROM DbOrder d WHERE d.orderDate = :orderDate")})
public class DbOrder implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "orderDate")
    @Temporal(TemporalType.DATE)
    private Date orderDate;
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    @ManyToOne
    private dbClient clientId;
    @OneToMany(mappedBy = "orderID",fetch = FetchType.EAGER)
    private Collection<DbDishOrder> dbDishOrderCollection;

    public double getOrderPrice() {
        orderPrice=0;
        if (dbDishOrderCollection!=null) {
            for (DbDishOrder dishOrder : dbDishOrderCollection) {
                orderPrice += dishOrder.getDishQuantity() * dishOrder.getDish().getPrice();
            }
        }
        return orderPrice;


    }

    public void setOrderPrice(double orderPrice) {
        this.orderPrice = orderPrice;
    }

    private double orderPrice;

    public DbOrder() {
    }

    public DbOrder(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public dbClient getClientId() {
        return clientId;
    }

    public void setClientId(dbClient clientId) {
        this.clientId = clientId;
    }

    @XmlTransient
    public Collection<DbDishOrder> getDbDishOrderCollection() {
        return dbDishOrderCollection;
    }

    public void setDbDishOrderCollection(Collection<DbDishOrder> dbDishOrderCollection) {
        this.dbDishOrderCollection = dbDishOrderCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DbOrder)) {
            return false;
        }
        DbOrder other = (DbOrder) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.restaurant.controller.util.DbOrder[ id=" + id + " ]";
    }

}
