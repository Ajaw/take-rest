package com.restaurant.rest.Service;

import com.restaurant.rest.Model.Collections.dbDishList;
import com.restaurant.rest.Model.dbDish;

/**
 * Created by Dominik on 06.09.2017.
 */
public interface DishREST {

    public abstract dbDishList get();

    public abstract dbDish postDish(dbDish dish);

    public abstract dbDish updateDish(dbDish dish);

}
