/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.restaurant.rest.Model;


import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Dominik
* @version 1.0
 */
@Entity
@Table(name = "dbIndegrientMagazine")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DbIndegrientMagazine.findAll", query = "SELECT d FROM DbIndegrientMagazine d")
    , @NamedQuery(name = "DbIndegrientMagazine.findById", query = "SELECT d FROM DbIndegrientMagazine d WHERE d.id = :id")
    , @NamedQuery(name = "DbIndegrientMagazine.findByMessureType", query = "SELECT d FROM DbIndegrientMagazine d WHERE d.messureType = :messureType")
    , @NamedQuery(name = "DbIndegrientMagazine.findByName", query = "SELECT d FROM DbIndegrientMagazine d WHERE d.name = :name")
    , @NamedQuery(name = "DbIndegrientMagazine.findByQuantity", query = "SELECT d FROM DbIndegrientMagazine d WHERE d.quantity = :quantity")})
public class DbIndegrientMagazine implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",unique = true)
    private Integer id;
    @Size(max = 255)
    @Column(name = "messureType")
    private String messureType;
    @Size(max = 255)
    @Column(name = "name")
    private String name;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "quantity")
    private Double quantity;
    @OneToMany(mappedBy = "indegrietnMagazineId")
    private Collection<dbDishIndegrient> dbDishIndegrientCollection;

    public DbIndegrientMagazine() {
    }

    public DbIndegrientMagazine(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessureType() {
        return messureType;
    }

    public void setMessureType(String messureType) {
        this.messureType = messureType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    @XmlTransient
    public Collection<dbDishIndegrient> getDbDishIndegrientCollection() {
        return dbDishIndegrientCollection;
    }

    public void setDbDishIndegrientCollection(Collection<dbDishIndegrient> dbDishIndegrientCollection) {
        this.dbDishIndegrientCollection = dbDishIndegrientCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DbIndegrientMagazine)) {
            return false;
        }
        DbIndegrientMagazine other = (DbIndegrientMagazine) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.restaurant.controller.util.DbIndegrientMagazine[ id=" + id + " ]";
    }

}
