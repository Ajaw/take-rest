package com.restaurant.rest.Service;

import com.restaurant.rest.Model.Collections.DbDishOrderList;
import com.restaurant.rest.Model.Collections.dbDishIndegrientList;
import com.restaurant.rest.Model.dbDish;
import com.restaurant.rest.Model.dbDishIndegrient;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by Dominik on 06.09.2017.
 */
@Local
public interface DishIndegrientREST {

    public abstract List<dbDishIndegrient> get();

    public abstract dbDish postDish(dbDishIndegrient dish);

    public abstract void updateDishIndegrient(dbDishIndegrientList dbDishIndegrientList);

    public abstract dbDishIndegrientList getIndegrientForDish(dbDish dish);

}
