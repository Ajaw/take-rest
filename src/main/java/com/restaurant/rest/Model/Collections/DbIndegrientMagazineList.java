package com.restaurant.rest.Model.Collections;

import com.restaurant.rest.Model.DbIndegrientMagazine;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Dominik on 16.09.2017.
 */
@XmlRootElement
public class DbIndegrientMagazineList {


    public DbIndegrientMagazineList(){

    }

    public DbIndegrientMagazineList(List<DbIndegrientMagazine> indegrientMagazines) {
        this.indegrientMagazines = indegrientMagazines;
    }

    @XmlElementWrapper
    @XmlElement(name="DbIndegrientMagazine")
    public List<DbIndegrientMagazine> getIndegrientMagazines() {
        return indegrientMagazines;
    }

    public void setIndegrientMagazines(List<DbIndegrientMagazine> indegrientMagazines) {
        this.indegrientMagazines = indegrientMagazines;
    }

    private List<DbIndegrientMagazine> indegrientMagazines;

}
