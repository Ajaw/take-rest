package com.restaurant.rest.EJB;

import com.restaurant.rest.Model.Collections.DbDishOrderList;
import com.restaurant.rest.Model.Collections.dbDishIndegrientList;
import com.restaurant.rest.Model.DbDishOrder;
import com.restaurant.rest.Model.dbDish;
import com.restaurant.rest.Model.dbDishIndegrient;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Dominik on 17.09.2017.
 */
@Stateless
public class DishIndegrientEJB {


    @PersistenceContext(unitName = "restaurant")
    EntityManager manager;

    public dbDishIndegrientList findAll() {
        Query q = manager.createQuery("select c from dbDishIndegrient c");
        List<dbDishIndegrient> results = q.getResultList();
        dbDishIndegrientList orderList = new dbDishIndegrientList(results);
        return orderList;
    }

    public dbDishIndegrient create(dbDishIndegrient order) {
        manager.persist(order);
        return order;
    }

    public dbDishIndegrient update(dbDishIndegrient order){
        order =  manager.merge(order);
        return  order;
    }

}
