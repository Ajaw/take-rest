package com.restaurant.rest.EJB;

import com.restaurant.rest.Model.Collections.DbDishOrderList;
import com.restaurant.rest.Model.Collections.DbOrderList;
import com.restaurant.rest.Model.DbDishOrder;
import com.restaurant.rest.Model.DbOrder;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Dominik on 18.09.2017.
 */
@Stateless
public class DishOrderEJB {

    @PersistenceContext(unitName = "restaurant")
    EntityManager manager;

    public DbDishOrderList findAll() {
        Query q = manager.createQuery("select c from DbOrder c");
        List<DbDishOrder> results = q.getResultList();
        DbDishOrderList orderList = new DbDishOrderList(results);
        return orderList;
    }

    public DbDishOrder create(DbDishOrder order) {
        manager.persist(order);
        return order;
    }

    public DbDishOrder update(DbDishOrder order){
        order =  manager.merge(order);
        return  order;
    }


}
