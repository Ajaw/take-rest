package com.restaurant.rest.Model.Collections;

import com.restaurant.rest.Model.dbClient;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Dominik on 16.09.2017.
 */
@XmlRootElement
public class dbClientList {

    public dbClientList(List<dbClient> clientList) {
        this.clientList = clientList;
    }

    public dbClientList() {
    }

    @XmlElementWrapper
    @XmlElement(name="dbClient")
    public List<dbClient> getClientList() {
        return clientList;
    }

    public void setClientList(List<dbClient> clientList) {
        this.clientList = clientList;
    }

    private List<dbClient> clientList;

}
