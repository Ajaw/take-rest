package com.restaurant.rest.Service;

import com.restaurant.rest.Model.Collections.DbDishOrderList;
import com.restaurant.rest.Model.Collections.DbIndegrientMagazineList;
import com.restaurant.rest.Model.DbDishOrder;
import com.restaurant.rest.Model.DbIndegrientMagazine;
import com.restaurant.rest.Model.DbOrder;

import java.util.List;

/**
 * Created by Dominik on 16.09.2017.
 */
public interface DishOrderREST {


    public abstract DbDishOrderList get();

    public abstract String postIndegrient(DbDishOrder dishOrder);

    public abstract void updateOrderDishes(DbDishOrderList dbDishOrderList);

    public abstract void update(DbDishOrder indegrientMagazine);

    public abstract DbDishOrderList getDishesForOrder(DbOrder order);

}
