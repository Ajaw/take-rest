package com.restaurant.rest.EJB;

import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import java.util.List;

/**
 * Created by Dominik on 06.09.2017.
 */
public abstract class AbstractFacade<T> {

    private Class<T> entityClass;

    public AbstractFacade(Class<T> entityClass){ this.entityClass = entityClass; }

    protected abstract EntityManager getEntityManager();

    public void create(T entity){
        getEntityManager().setFlushMode(FlushModeType.COMMIT);
        getEntityManager().persist(entity);
        getEntityManager().flush();
    }

    public void edit(T entity){
        getEntityManager().setFlushMode(FlushModeType.COMMIT);
        getEntityManager().merge(entity);
        getEntityManager().flush();
    }

    public void remove(T entity){
        getEntityManager().setFlushMode(FlushModeType.COMMIT);
        getEntityManager().remove(entity);
        getEntityManager().flush();
    }

    public T find(Object id){return getEntityManager().find(entityClass,id);}

    public List findAll(){
        return getEntityManager().createNamedQuery(entityClass.getSimpleName()+".findAll").getResultList();
    }

}
