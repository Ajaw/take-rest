package com.restaurant.rest.EJB;

import com.restaurant.rest.Model.Collections.dbClientList;
import com.restaurant.rest.Model.Collections.dbDishList;
import com.restaurant.rest.Model.dbClient;
import com.restaurant.rest.Model.dbDish;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Dominik on 17.09.2017.
 */
@Stateless
public class DishEJB {

    @PersistenceContext(unitName = "restaurant")
    EntityManager manager;

    public dbDishList findAll() {
        Query q = manager.createQuery("select c from dbDish c");
        List<dbDish> results = q.getResultList();
        dbDishList clientList = new dbDishList(results);
        return clientList;
    }

    public dbDish create(dbDish dish) {
        manager.persist(dish);
        return dish;
    }

    public dbDish update(dbDish dish){
        dish =  manager.merge(dish);
        return  dish;
    }

    public dbDish findDishById(int id){
        Query q = manager.createNamedQuery("dbDish.findById");
        q.setParameter("id",id);
        try {
            return (dbDish) q.getSingleResult();
        }
        catch (NoResultException ex){
            System.out.print(ex.getMessage());
        }
        return null;
    }

}
