package com.restaurant.rest.Service.Impl;

import com.restaurant.rest.EJB.DishOrderEJB;
import com.restaurant.rest.EJB.OrderEJB;
import com.restaurant.rest.Model.Collections.DbDishOrderList;
import com.restaurant.rest.Model.Collections.DbOrderList;
import com.restaurant.rest.Model.Collections.dbDishIndegrientList;
import com.restaurant.rest.Model.DbDishOrder;
import com.restaurant.rest.Model.DbOrder;
import com.restaurant.rest.Model.dbDish;
import com.restaurant.rest.Service.DishOrderREST;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Dominik on 16.09.2017.
 */

@Path("/dishorder")
@Produces({"application/xml"})
@Consumes({"application/xml"})
public class DishOrderRESTImpl implements DishOrderREST {


    @EJB
    private DishOrderEJB dishOrderEJB;
    @EJB
    private OrderEJB orderEJB;

    @Override
    public DbDishOrderList get() {
        return dishOrderEJB.findAll();
    }

    @Override
    public String postIndegrient(DbDishOrder dishOrder) {
         dishOrderEJB.create(dishOrder);
         return "succes";
    }

    @Override
    @POST
    @Path("/bulkupdate")
    public void updateOrderDishes(DbDishOrderList dbDishOrderList) {

        for (DbDishOrder dishOrder : dbDishOrderList.getDishOrders()){
            dishOrderEJB.update(dishOrder);
        }

    }

    @Override
    @POST
    @Path("/getdishesfororder")
    public DbDishOrderList getDishesForOrder(DbOrder order) {
        DbOrder newOrder =  orderEJB.findOrderById(order.getId());
        DbDishOrderList indegrientList = new DbDishOrderList(
                new LinkedList<>(newOrder.getDbDishOrderCollection()));
        return indegrientList;
    }

    @Override
    @POST
    @Path("/update")
    public void update(DbDishOrder indegrientMagazine) {

    }
}
