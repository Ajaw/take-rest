package com.restaurant.rest.Model.Collections;

import com.restaurant.rest.Model.DbDishOrder;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Dominik on 18.09.2017.
 */
@XmlRootElement
public class DbDishOrderList {

    public DbDishOrderList() {
    }

    public DbDishOrderList(List<DbDishOrder> dishOrders) {

        this.dishOrders = dishOrders;
    }

    @XmlElementWrapper
    @XmlElement(name="DbDishOrder")
    public List<DbDishOrder> getDishOrders() {

        return dishOrders;
    }

    public void setDishOrders(List<DbDishOrder> dishOrders) {
        this.dishOrders = dishOrders;
    }

    private List<DbDishOrder> dishOrders;

}
