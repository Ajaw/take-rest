package com.restaurant.rest.Service.Impl;

import com.restaurant.rest.EJB.IndegrientMagazineEJB;
import com.restaurant.rest.Model.Collections.DbIndegrientMagazineList;
import com.restaurant.rest.Model.DbIndegrientMagazine;
import com.restaurant.rest.Service.IndegrientMagazineREST;

import javax.ejb.EJB;
import javax.ws.rs.*;

/**
 * Created by Dominik on 16.09.2017.
 */
@Path("/indegrient_mag")
@Produces({"application/xml"})
@Consumes({"application/xml"})
public class IndegrientMagazineRESTImpl implements IndegrientMagazineREST {
    @EJB
    private IndegrientMagazineEJB magazineEJB;

    @Override
    @GET
    @Path("/get")
    public DbIndegrientMagazineList get() {
        return  magazineEJB.findAll();
    }

    @Override
    @POST
    @Path("/create")
    public DbIndegrientMagazine postIndegrient(DbIndegrientMagazine indegrientMagazine) {
        try {
          return   magazineEJB.create(indegrientMagazine);
        }
        catch (Exception ex ){
            System.out.print(ex.getMessage());
        }
        return null;
    }

    @Override
    @POST
    @Path("/update")
    public DbIndegrientMagazine update(DbIndegrientMagazine indegrientMagazine) {
            return magazineEJB.update(indegrientMagazine);
    }
}
