package com.restaurant.rest.EJB;

import com.restaurant.rest.Model.Collections.DbOrderList;
import com.restaurant.rest.Model.Collections.dbDishList;
import com.restaurant.rest.Model.DbOrder;
import com.restaurant.rest.Model.dbDish;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Dominik on 17.09.2017.
 */
@Stateless
public class OrderEJB {


    @PersistenceContext(unitName = "restaurant")
    EntityManager manager;

    public DbOrderList findAll() {
        Query q = manager.createQuery("select c from DbOrder c");
        List<DbOrder> results = q.getResultList();
        DbOrderList orderList = new DbOrderList(results);
        return orderList;
    }

    public DbOrder create(DbOrder order) {
        manager.persist(order);
        return order;
    }

    public DbOrder update(DbOrder order){
        order =  manager.merge(order);
        return  order;
    }

    public DbOrder findOrderById(int id){
        Query q = manager.createNamedQuery("DbOrder.findById");
        q.setParameter("id",id);
        try {
            return (DbOrder) q.getSingleResult();
        }
        catch (NoResultException ex){
            System.out.print(ex.getMessage());
        }
        return null;
    }

}
