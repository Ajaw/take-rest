package com.restaurant.rest.Model.Collections;

import com.restaurant.rest.Model.DbOrder;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Dominik on 17.09.2017.
 */
@XmlRootElement
public class DbOrderList {

    public DbOrderList(List<DbOrder> orders) {
        this.orders = orders;
    }

    public DbOrderList() {
    }


    @XmlElementWrapper
    @XmlElement(name="DbOrder")
    public List<DbOrder> getOrders() {
        return orders;
    }

    public void setOrders(List<DbOrder> orders) {
        this.orders = orders;
    }

    private List<DbOrder> orders;


}
