package com.restaurant.rest.EJB;

import com.restaurant.rest.Model.Collections.dbClientList;
import com.restaurant.rest.Model.dbClient;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Dominik on 05.09.2017.
 */
@Stateless
public class ClientEJB  {
    @PersistenceContext(unitName = "restaurant")
    EntityManager manager;

    public dbClientList findAll() {
        Query q = manager.createQuery("select c from dbClient c");
        List<dbClient> results = q.getResultList();
        dbClientList clientList = new dbClientList(results);
        return clientList;
    }

    public dbClient create(dbClient client) {
        manager.persist(client);
        return client;
    }

    public dbClient update(dbClient client){
        client =  manager.merge(client);
        return  client;
    }

}
