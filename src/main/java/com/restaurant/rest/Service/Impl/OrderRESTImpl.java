package com.restaurant.rest.Service.Impl;

import com.restaurant.rest.EJB.OrderEJB;
import com.restaurant.rest.Model.Collections.DbOrderList;
import com.restaurant.rest.Model.DbOrder;
import com.restaurant.rest.Service.OrderREST;

import javax.ejb.EJB;
import javax.ws.rs.*;

/**
 * Created by Dominik on 16.09.2017.
 */
@Path("/order")
@Produces({"application/xml"})
@Consumes({"application/xml"})
public class OrderRESTImpl implements OrderREST {

    @EJB
    private OrderEJB orderEJB;

    @Override
    @GET
    @Path("/get")
    public DbOrderList get() {
        return orderEJB.findAll();
    }

    @Override
    @POST
    @Path("/create")
    public DbOrder postOrder(DbOrder order) {
       return orderEJB.create(order);

    }

    @Override
    @POST
    @Path("/update")
    public DbOrder update(DbOrder order) {
        return orderEJB.update(order);
    }
}
