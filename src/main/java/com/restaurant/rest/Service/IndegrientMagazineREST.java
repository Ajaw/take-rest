package com.restaurant.rest.Service;

import com.restaurant.rest.Model.Collections.DbIndegrientMagazineList;
import com.restaurant.rest.Model.DbIndegrientMagazine;

/**
 * Created by Dominik on 16.09.2017.
 */
public interface IndegrientMagazineREST {

    public abstract DbIndegrientMagazineList get();

    public abstract DbIndegrientMagazine postIndegrient(DbIndegrientMagazine client);

    public abstract DbIndegrientMagazine update(DbIndegrientMagazine indegrientMagazine);

}
