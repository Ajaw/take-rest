package com.restaurant.rest.Service;

import com.restaurant.rest.Model.Collections.DbOrderList;
import com.restaurant.rest.Model.Collections.dbClientList;
import com.restaurant.rest.Model.DbOrder;
import com.restaurant.rest.Model.dbClient;

import java.util.List;

/**
 * Created by Dominik on 16.09.2017.
 */
public interface OrderREST {

    public abstract DbOrderList get();

    public abstract DbOrder postOrder(DbOrder order);

    public abstract DbOrder update(DbOrder order);
}
