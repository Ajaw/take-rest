package com.restaurant.rest.Service.Impl;

import com.restaurant.rest.EJB.ClientEJB;

import com.restaurant.rest.Model.Collections.dbClientList;
import com.restaurant.rest.Model.dbClient;
import com.restaurant.rest.Service.ClientREST;

import javax.ejb.EJB;
import javax.ws.rs.*;

/**
 * Created by Dominik on 05.09.2017.
 */
@Path("/client")
@Produces({"application/xml"})
@Consumes({"application/xml"})
public class ClientRESTImpl implements ClientREST {

    @EJB
    ClientEJB clientEJB;

    @Override
    @GET
    @Path("/get")
    public dbClientList get() {

        return clientEJB.findAll();

    }

    @Override
    @POST
    @Path("/create")
    public dbClient postClient(dbClient client) {
        try {
            return clientEJB.create(client);
        }
        catch (Exception ex){
            return null;
        }
    }
    @Override
    @POST
    @Path("/update")
    public dbClient update(dbClient client) {
        return clientEJB.update(client);
    }


}
