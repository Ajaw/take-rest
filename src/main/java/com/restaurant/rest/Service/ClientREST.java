package com.restaurant.rest.Service;



import com.restaurant.rest.Model.Collections.dbClientList;
import com.restaurant.rest.Model.dbClient;

/**
 * Created by Dominik on 05.09.2017.
 */
public interface ClientREST  {

    public abstract dbClientList get();

    public abstract dbClient postClient(dbClient client);

    public abstract dbClient update(dbClient client);



}
