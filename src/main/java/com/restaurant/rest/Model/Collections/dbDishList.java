package com.restaurant.rest.Model.Collections;

import com.restaurant.rest.Model.dbDish;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Dominik on 17.09.2017.
 */
@XmlRootElement
public class dbDishList {
    public dbDishList(List<dbDish> dbDishList) {
        this.dbDishList = dbDishList;
    }

    public dbDishList() {
    }

    @XmlElementWrapper
    @XmlElement(name="dbDish")
    public List<dbDish> getDbDishList() {
        return dbDishList;
    }

    public void setDbDishList(List<dbDish> dbDishList) {
        this.dbDishList = dbDishList;
    }

    private List<dbDish> dbDishList;

}
