package com.restaurant.rest.EJB;

import com.restaurant.rest.Model.Collections.DbIndegrientMagazineList;
import com.restaurant.rest.Model.DbIndegrientMagazine;
import com.restaurant.rest.Model.dbClient;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Dominik on 16.09.2017.
 */
@Stateless
public class IndegrientMagazineEJB {
    @PersistenceContext(unitName = "restaurant")
    EntityManager manager;

    public DbIndegrientMagazineList findAll() {
        Query q = manager.createQuery("select c from DbIndegrientMagazine c");

        List<DbIndegrientMagazine> results = q.getResultList();
        DbIndegrientMagazineList list = new DbIndegrientMagazineList(results);
        return list;
    }

    public DbIndegrientMagazine create(DbIndegrientMagazine client) {

        manager.persist(client);
        return client;
    }

    public DbIndegrientMagazine update(DbIndegrientMagazine indegrientMagazine){
       return manager.merge(indegrientMagazine);
    }

}
